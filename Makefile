all:
	nasm -f bin ./boot/main.asm -o ./boot/boot
	nasm -I./stage2/include/ -f bin ./stage2/main.asm -o ./stage2/stage2.sys
	dd if=./boot/boot of=./floppy.flp conv=notrunc
	sleep 1
	sudo mount ./floppy.flp /mnt/floppy  -t vfat -o loop,umask=0
	cp ./stage2/stage2.sys /mnt/floppy/
	sync
	sleep 1
	sudo umount /mnt/floppy
	./start.sh
