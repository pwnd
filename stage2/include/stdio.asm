puts:
	push si
  mov si,ax
.loop:
	mov al,[si]
	test al,al
	jz .end
  call putc
	inc si
	jmp .loop
.end:
	pop si
  xor ax,ax
	ret

gets:
  push di
  mov di,ax
.loop:
  call getc
  cmp al,13
  je .end
  call putc
  mov [di],al
  inc di
  jmp .loop
.end:  
  mov byte [di],0
  mov ax,di
  pop di
  ret

putc:
  mov ah,0x0e
  int 0x10
  xor ax,ax
  ret

getc:
  xor ax,ax
  int 0x16
  ret
