bits 16
org 0x0

jmp start

%include "stdio.asm"

start:
  cli
	mov ax,cs
  mov ss,ax
  mov sp,0xffff

  push cs
	pop ds
  sti

	mov ax,load
	call puts
  
  mov ax,prompt
  call puts
  
  call gets

	cli
	hlt

load db "Stage2 loading...",13,10,0
prompt db "> ",0
buff db 0
