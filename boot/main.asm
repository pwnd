bits	16							
org		0x7c00						

start:          jmp loader				


TIMES 0Bh-$+start DB 0

bpbBytesPerSector:  	DW 512
bpbSectorsPerCluster: 	DB 1
bpbReservedSectors: 	DW 1
bpbNumberOfFATs: 	    DB 2
bpbRootEntries: 	    DW 224
bpbTotalSectors: 	    DW 2880
bpbMedia: 	            DB 0xF0
bpbSectorsPerFAT: 	    DW 9
bpbSectorsPerTrack: 	DW 18
bpbHeadsPerCylinder: 	DW 2
bpbHiddenSectors: 	    DD 0
bpbTotalSectorsBig:     DD 0
bsDriveNumber: 	        DB 0
bsUnused: 	            DB 0
bsExtBootSignature: 	DB 0x29
bsSerialNumber:	        DD 0xa0a1a2a3
bsVolumeLabel: 	        DB "MOS FLOPPY "
bsFileSystem: 	        DB "FAT12   "


Print:
	pusha
	mov ah,0x0e
.loop:
	mov al,[si]
	test al,al
	jz .end
	int 0x10
	inc si
	jmp .loop
.end:
	popa
	ret


LBAtoCHS:
	pusha

	xor dx,dx
	div word [bpbSectorsPerTrack]
	inc dl
	mov byte [absSector],dl

	xor dx,dx
	div word [bpbHeadsPerCylinder]
	mov byte [absHead],dl

	mov byte [absTrack],al
	popa
	ret

;CX Count
;ES:BX offset
Read:	

.read:
	call Reset

	push ax
	push bx
	push cx
	
	call LBAtoCHS
	mov ah,0x02
	xor dl,dl
	mov dh,[absHead]
	mov ch,[absTrack]
	mov cl,[absSector]
	mov al,1

	int 0x13
	pop cx
	pop bx
	pop ax
	jc .read
	dec cx
	add bx,word [bpbBytesPerSector]
	inc ax
	test cx,cx
	jnz .read	
	ret

Reset:
	pusha

.reset:
	mov ah,0
	mov dl,0
	int 0x13
	jc .reset

	popa
	ret

ClusterLBA:	
	sub ax,2
	xor cx,cx
	mov cl,byte [bpbSectorsPerCluster]
	mul cx
	add ax,word [data]	
	ret

loader:
	
	cli			
	mov ax,cs
	mov ds,ax	
	sti

	mov ax,0x0003
	int 0x10

	mov si,banner
	call Print
	
	xor cx,cx
	xor dx,dx
	mov ax,0x0020
	mul word [bpbRootEntries]
	div word [bpbBytesPerSector]
	xchg ax,cx

	mov al,[bpbNumberOfFATs]
	mul word [bpbSectorsPerFAT]
	add ax,[bpbReservedSectors]
	
	mov word [data],ax
	add word [data],cx

	push 0x0000
	pop es
	mov bx,0x7e00
	
	call Read

	mov cx,[bpbRootEntries]
	mov di,0x7e00
.loop:
	push cx
	mov cx,11
	mov si,imageName
	push di
	rep cmpsb
	pop di
	je .ok
	pop cx
	add di,32
	loop .loop
	jmp .bad

.ok:
	mov si,ok
	call Print
	mov dx,word [di+0x1A]
	mov word [Cluster],dx

	xor ax,ax
	mov al,[bpbNumberOfFATs]
	mul word [bpbSectorsPerFAT]
	xchg ax,cx

	mov ax,[bpbReservedSectors]

	push 0x0000
	pop es
	mov bx,0x7e00

	call Read
	

	push 0x2000
	pop es
	xor bx,bx
	push bx
	push es

.load:
	pop es
	pop bx

	mov ax,word [Cluster]
	call ClusterLBA
	mov cx,1
	call Read
	push bx
	push es

	mov ax,word [Cluster]
	mov cx,ax
	mov dx,ax
	shr dx,0x0001
	add cx,dx
	push 0x0
	pop es
	mov bx,0x7e00
	add bx,cx
	mov dx,word [bx]
	test ax,0x0001
	jnz .odd
.even:
	and dx,0000111111111111b
	jmp .done
.odd:
	shr dx,0x0004
.done:
	mov word [Cluster],dx
	cmp dx,0x0FF0
	jb .load
	
	

	jmp 0x2000:0
	jmp .end
.bad:
	mov si,bad
	call Print
.end:
	cli
	hlt		
	
	absSector db 0
	absHead db 0
	absTrack db 0
	Cluster dw 0
	data dw 0
	imageName db "STAGE2  SYS"
	ok db "Stage2 was find...",13,10,0
	bad db "Stage2 dont find...",13,10,0
	banner db "Boot start...",13,10,0
	times 510 - ($-$$) db 0		
	dw 0xAA55			
